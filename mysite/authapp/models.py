from django.db import models


class Role(models.Model):
    ADMIN = 1
    REGULAR_USER = 2
    ROLE_CHOICES = (
      (ADMIN, 'admin'),
      (REGULAR_USER, 'regular_user'),
    )

    id = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, primary_key=True)

    def __str__(self):
        return self.get_id_display()


class AppUser(models.Model):    
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    email = models.EmailField(max_length=50)
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    birth_date = models.DateTimeField('birthday')
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    active = models.BooleanField(default=True)


class TemporaryBanIp(models.Model):
    id = models.AutoField(primary_key=True)
    ip_address = models.GenericIPAddressField("IP")
    blocked = models.BooleanField(default=True)

    def __str__(self):
        return self.ip_address


class IpRequest(models.Model):
    ip_address = models.GenericIPAddressField("IP")
    attempt_time = models.IntegerField(blank=0)

    def __str__(self):
        return self.ip_address

