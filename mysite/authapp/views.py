from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.core.mail import EmailMessage
from django.views.generic import View
from django.views.generic import ListView

from .models import AppUser, Role, TemporaryBanIp
from .decorators import banned_ip_address, user_logged_in, save_ip_request


from fuzzywuzzy import fuzz
from ratelimit.decorators import ratelimit


@ratelimit(key='ip', rate='2/m')
def index(request):
    return render(request, 'authapp/index.html')


def logout(request):

    return render(request, 'authapp/index.html')


class LoginView(View):

    def get(self, request):
        return render(request, 'authapp/login.html')

    @save_ip_request
    @banned_ip_address
    def post(self, request):
        try:
            user = AppUser.objects.get(
                username=request.POST['username'],
                password=request.POST['password'])
            if not user.active:
                return render(request, 'authapp/login.html', {
                    'error_message': "User account is not activated! PLease verify you email.",
                })
        except AppUser.DoesNotExist:
            return render(request, 'authapp/login.html', {
                'error_message': "Wrong username and/or password!",
            })
        else:
            if user.role == Role.REGULAR_USER:
                return HttpResponseRedirect(reverse('authapp:detail', args=(user.id,)))
            else:
                return HttpResponseRedirect(reverse('authapp:admin', args=(user.id,)))

class RegisterView(View):

    def get(self, request):
        return render(request, 'authapp/register.html')

    def post(self, request):
        try:
            user = AppUser.objects.get(
                username=request.POST['username'],
                password=request.POST['password'],
                role=Role.REGULAR_USER)
            if not user.active:
                user.delete()
            else:
                return render(request, 'authapp/login.html', {
                    'error_message': "User already exists!",
                })
        except AppUser.DoesNotExist:
            pass

        user = AppUser(
            email=request.POST['email'],
            first_name=request.POST['first_name'],
            last_name=request.POST['last_name'],
            birth_date=request.POST['birth_date'],
            username=request.POST['username'],
            password=request.POST['password'],
            role=Role(Role.REGULAR_USER)
        )

        user.save()

        send_email_to_user(user, site_domain=get_current_site(request), end_point='activate_email')

        return HttpResponse('Please confirm your email address to complete the registration')


class RegularUsersListView(ListView):
    model = AppUser
    template_name = 'appuser_list.html'


def send_email_to_user(user, site_domain, end_point):

    mail_subject = 'Activate your account!'

    message = render_to_string('authapp/'+end_point+'.html', {
        'user': user,
        'domain': site_domain.domain,
        'uid': str(urlsafe_base64_encode(force_bytes(user.password)))[2:-1],
    })
    email = EmailMessage(
        mail_subject, message, to=[user.email]
    )
    email.send()


class RecoverPasswordView(View):

    def get(self, request):
        return render(
            request,
            'authapp/recover_password_email.html')

    def post(self, request):
        try:
            user = AppUser.objects.get(email=request.POST['email'])

        except AppUser.DoesNotExist:
            return render(
                request,
                'authapp/reset_password_email.html',
                {'message': "There is no active account with this email!"}
            )

        send_email_to_user(user, site_domain=get_current_site(request), end_point='recover_pass')

        return render(request, 'authapp/login.html', {
            'message': 'Please verify you inbox. An email was send to you!'})


class ResetPasswordView(View):

    def get(self, request, uidb64=None):

        global uuid
        uuid = uidb64

        return render(
            request,
            'authapp/password_reset.html')

    def post(self, request):
        global uuid

        if request.POST['new_password'] != request.POST['new_password_confirmed']:
            return render(
                request,
                'authapp/password_reset.html',
                {'message': "Passwords do not match!"}
            )

        uid = force_text(urlsafe_base64_decode(uuid))
        user = AppUser.objects.get(password=uid)

        if fuzz.ratio(user.password, request.POST['new_password_confirmed']) > 80:
            return render(
                request,
                'authapp/password_reset.html',
                {'message': "This very similar to the old password!"}
            )
        else:
            user.password = request.POST['new_password']
            user.save()

            return render(request, 'authapp/login.html', {
                'message': "Your password has been successfully changed!",
            })


def activate(request, uidb64):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = AppUser.objects.get(password=uid)
    except(TypeError, ValueError, OverflowError, AppUser.DoesNotExist):
        user = None
    if user is not None:
        user.active = True
        user.save()
        # return redirect('home')
        return render(request, 'authapp/login.html', {
            'message': "Thank you for your email confirmation. Now you can login to your account!",
        })
    else:
        return HttpResponse('Activation link is invalid!')


def detail(request, user_id):
    user = get_object_or_404(AppUser, pk=user_id)

    return render(request, 'authapp/detail.html', {'user': user})


def send_email(request):
    try:
        user = AppUser.objects.get(email=request.POST['email'])

    except AppUser.DoesNotExist:
        return render(
            request,
            'authapp/reset_password_email.html',
            {'message': "There is no active account with this email!"}
        )

    send_email_to_user(user, site_domain=get_current_site(request), end_point='forgot_password')

    return HttpResponse('Please verify you inbox. An email was send to you with a password reset link!')


def forgot_email(request):
    return render(
        request,
        'authapp/reset_password_email.html')


uuid = None


@user_logged_in
def change_password(request, user_id):

    return render(
        request,
        'authapp/change_password.html',
        {'user_id': user_id},
        {'mesagee':"sothmfds"}
    )


@user_logged_in
def password_change(request, user_id):
    user = get_object_or_404(AppUser, pk=user_id)

    if user.password != request.POST['old_password']:
        return render(
            request,
            'authapp/change_password.html',
            {'user_id': user_id, 'message': "Wrong old password!"}
        )

    if request.POST['new_password'] != request.POST['new_password_confirmed']:
        return render(
            request,
            'authapp/change_password.html',
            {'user_id': user_id, 'message': "Passwords do not match!"}
        )

    user.save()

    return render(
        request,
        'authapp/edit_profile.html',
        {'user': user, 'message': "updated!"}
    )


@user_logged_in
def edit_user_profile(request, user_id):
    user = get_object_or_404(AppUser, pk=user_id)

    return render(
        request,
        'authapp/edit_profile.html',
        {'user': user}
    )

@user_logged_in
def update(request, user_id):
    user = get_object_or_404(AppUser, pk=user_id)

    if request.POST['first_name'] and request.POST['first_name'] != user.first_name:
        user.first_name = request.POST['first_name']
    if request.POST['last_name'] and request.POST['last_name'] != user.last_name:
        user.last_name = request.POST['last_name']
    if request.POST['email'] and request.POST['email'] != user.email:
        user.email = request.POST['email']
    if request.POST['birth_date'] and request.POST['birth_date'] != user.birth_date:
        user.birth_date = request.POST['birth_date']

    user.save()

    return HttpResponseRedirect(reverse('authapp:detail',
                                        args=(user_id,)))


def admin(request, user_id):
    global userid
    userid = user_id
    user = get_object_or_404(AppUser, pk=user_id)
    return render(
        request,
        'authapp/admin.html',
        {'users': AppUser.objects.all(), 'user': user, 'banned_ip': TemporaryBanIp.objects.values('ip_address').distinct()}
    )


def unblock(request, ip):
    TemporaryBanIp.objects.filter(ip_address=ip).delete()
    user = get_object_or_404(AppUser, pk=userid)

    return render(
        request,
        'authapp/admin.html',
        {'users': AppUser.objects.all(), 'user': user, 'banned_ip': TemporaryBanIp.objects.values('ip_address').distinct()}
    )
