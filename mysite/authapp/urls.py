from django.urls import path
from . import views

app_name = 'authapp'

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.logout, name='logout'),
    path('register/', views.RegisterView.as_view(), name='register'),
    path('detail/<user_id>/', views.detail, name='detail'),
    path('admin/<user_id>', views.admin, name='admin'),
    path('edit_user_profile/<user_id>/', views.edit_user_profile, name='edit_user_profile'),
    path('update/<user_id>/', views.update, name='update'),
    path('change_password/<user_id>/', views.change_password, name='change_password'),
    path('password_change/<user_id>/', views.change_password, name='password_change'),
    path('activate/<uidb64>/',views.activate, name='activate'),
    path('forgot_email/', views.forgot_email, name='forgot_email'),
    path('send_email/', views.send_email, name='send_email'),
    path('password_reset/', views.ResetPasswordView.as_view(), name='password_reset'),
    path('password_reset/<uidb64>', views.ResetPasswordView.as_view(), name='password_reset'),
    path('recover_password/', views.RecoverPasswordView.as_view(), name="recover_password"),
    path('users/', views.RegularUsersListView.as_view()),
    path('unblock/<ip>/', views.unblock, name='unblock')
]
