from django.shortcuts import render

from .models import AppUser, TemporaryBanIp, IpRequest

import time


def user_logged_in(function):
    def wrap(request, *args, **kwargs):
        try:
            AppUser.objects.get(pk=kwargs['user_id'])

            return function(request, *args, **kwargs)
        except (AppUser.DoesNotExist, KeyError):
            return render(request, 'authapp/index.html', {
                'error_message': "User not logged in!",
            })
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def save_ip_request(function):
    def wrap(self, request, *args, **kwargs):

        ip = request.META["REMOTE_ADDR"]

        IpRequest.objects.create(
            ip_address=ip,
            attempt_time=time.time()
        )

        return function(self, request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def banned_ip_address(function):
    def wrap(self, request, *args, **kwargs):
        ip = request.META["REMOTE_ADDR"]

        try:
            TemporaryBanIp.objects.get(ip_address=ip)
            return render(request, 'authapp/index.html', {
                'error_message': "IP address is banned!",
            })
        except TemporaryBanIp.DoesNotExist:
            pass

        last_ten_min_requests = IpRequest.objects.filter(
                ip_address=ip,
                attempt_time__gte=time.time()-10
        )

        if len(last_ten_min_requests) > 5:
            TemporaryBanIp.objects.get_or_create(defaults={'ip_address': ip}, ip_address=ip)
            return render(request, 'authapp/index.html', {
                'error_message': "IP address is banned!",
            })
        return function(self, request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
